﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using LibraryApp.Controllers;
using LibraryApp.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace LibraryTests
{
    [TestClass]
    public class GuestPageControllerTest
    {
        [TestMethod]
        public void TestIndexActionResultReturnsIndexView()
        {
            var controller = new GuestPageController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("Index", result.ViewName);
        }

        [TestMethod]
        public void TestIndexReturnsCorrectModel()
        {
            var controller = new GuestPageController();
            var result = controller.Index() as ViewResult;
            
            Report expected = new Report();
            expected.AuthorName = "A. Smith";
            expected.Date = DateTime.Now.ToString();
            expected.ReportText = "Report 1 Some text";

            Report actual = (result.Model as List<Report>).ToArray()[0];

            Assert.IsTrue(expected.AuthorName.Equals(actual.AuthorName) && expected.ReportText.Equals(actual.ReportText));
        }

        [TestMethod]
        public void TestAddReportReturnsCorrectView()
        {
            Report expected = new Report();
            expected.AuthorName = "A. Smith";
            expected.Date = DateTime.Now.ToString();
            expected.ReportText = "Report 1 Some text";

            var controller = new GuestPageController();
            var result = controller.AddReport("name", "text",new List<Report> { expected}) as ViewResult;
            Assert.AreEqual("Index", result.ViewName);
        }
    }



    [TestClass]
    public class MainPageControllerTest 
    {
        [TestMethod]
        public void TestMainPageControllerIndexActionReturnsIndexView()
        {
            var controller = new MainPageController();
            var actualView = controller.Index() as ViewResult;
            Assert.AreEqual("Index", actualView.ViewName);
        }
    
    }

    [TestClass]
    public class QuestionaryControllerTest
    {
        [TestMethod]
        public void TestQuestionaryControllerIndexActionReturnsIndexView()
        {
            var controller = new QuestionaryController();
            var actualView = controller.Index() as ViewResult;
            Assert.AreEqual("Index", actualView.ViewName);
        }

    }

}

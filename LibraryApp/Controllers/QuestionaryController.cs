﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.Controllers
{
    public class QuestionaryController : Controller
    {
        // GET: Questionary
        public ActionResult Index()
        {
            return View("Index");
        }
        
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            ViewBag.email = form["UserEmail"] as string;
            ViewBag.name = form["UserName"] as string;
            ViewBag.html = form["HTML"];
            ViewBag.css = form["CSS"];
            ViewBag.js = form["JS"];
            ViewBag.gender = form["Gender"];
            
            return View("QuestionaryResult"); ;
        }
    }
}